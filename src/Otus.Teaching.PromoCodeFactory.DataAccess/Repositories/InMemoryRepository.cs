﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public async Task<Guid> CreateAsync(T model)
        {
            var id = Guid.NewGuid();
            model.Id = id;
            await Task.Run(() => ((IList<T>)Data).Add(model));

            return id;
        }

        public async Task DeleteAsync(T model)
        {
            await DeleteAsync(model.Id);
        }

        public async Task DeleteAsync(Guid id)
        {
            var item = await GetByIdAsync(id);
            if (item == null)
            {
                throw new NullReferenceException("Запись не найдена");
            }

            await Task.Run(() => ((IList<T>)Data).Remove(item));
        }

        public async Task UpdateAsync(T model)
        {
            await DeleteAsync(model);
            await CreateAsync(model);
        }
    }
}