﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class AddEmployeeDTO
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public IEnumerable<Guid> Roles { get; set; }
    }

    public class UpdateEmployeeDTO : AddEmployeeDTO
    {
        public Guid Id { get; set; }
    }

}
