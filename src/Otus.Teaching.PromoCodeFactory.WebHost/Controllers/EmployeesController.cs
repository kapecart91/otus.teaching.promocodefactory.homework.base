﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _roleRepository;

        public EmployeesController(IRepository<Employee> employeeRepository,
                                   IRepository<Role> roleRepository)
        {
            _employeeRepository = employeeRepository;
            _roleRepository = roleRepository;
        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x =>
                new EmployeeShortResponse()
                {
                    Id = x.Id,
                    Email = x.Email,
                    FullName = x.FullName,
                }).ToList();

            return employeesModelList;
        }

        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Создает сотрудника
        /// </summary>
        /// <returns>Id сотрудника</returns>
        [HttpPost]
        public async Task<ActionResult<Guid>> Create(AddEmployeeDTO employee)
        {
            var model = Map(employee);
            var id = await _employeeRepository.CreateAsync(model);

            return id;
        }

        /// <summary>
        /// Удаляет сотрудника по Id
        /// </summary>
        [HttpDelete("{id:guid}")]
        public async Task Delete(Guid id)
        {
            await _employeeRepository.DeleteAsync(id);
        }

        /// <summary>
        /// Обновляет данные сотрудника
        /// </summary>
        [HttpPut]
        public async Task Update(UpdateEmployeeDTO employee)
        {
            if (employee == null && employee.Id == null)
            {
                throw new ArgumentNullException("Сотрудник не должен быть пустой, Id не должно быть null");
            }

            var model = Map(employee);
            model.Id = employee.Id;
            await _employeeRepository.UpdateAsync(model);
        }

        private Employee Map(AddEmployeeDTO employee)
        {
            return new Employee()
            {
                FirstName = employee.FirstName,
                Email = employee.Email,
                LastName = employee.LastName,
                Roles = employee.Roles.Select(r =>
                {
                    var role = _roleRepository.GetByIdAsync(r).Result;
                    if (role == null)
                    {
                        throw new NullReferenceException("Роль не найдена");
                    }

                    return _roleRepository.GetByIdAsync(r).Result;
                }).ToList()
            };
        }
    }
}